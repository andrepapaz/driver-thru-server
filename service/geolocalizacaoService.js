var restify = require('restify-clients');

function GeolocalizacaoService() {
    this._cliente = restify.createJsonClient({
        url: `http://webapidtv.azurewebsites.net`
    });
    this._cliente.basicAuth('AppV3rdtUs3r', 'Vj-Nsd@f6gh#541c%jMx*MlY');
}

GeolocalizacaoService.prototype.inserirGeolocalizacao = function(postData, callback) {

    let path = '/api/GeoLocalizacao/InserirGeolocalizacao';

    console.log(`Chamando api Driver-Thru: POST URL: ${this._cliente.url.hostname}${path} com o seguinte body: ${JSON.stringify(postData)}`);

    this._cliente.post(path, postData, callback);
}

module.exports = function() {
    return GeolocalizacaoService;
}