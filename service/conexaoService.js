var restify = require('restify-clients');

function ConexaoService() {
    this._cliente = restify.createJsonClient({
        url: `http://webapidtv.azurewebsites.net`
    });
    this._cliente.basicAuth('AppV3rdtUs3r', 'Vj-Nsd@f6gh#541c%jMx*MlY');
}

ConexaoService.prototype.listarConexoesAtivas = function(userID, callback) {

    let path = '/api/Conexao/ListarConexoesAtivas?usuarioId=' + userID;

    console.log(`Chamando api Driver-Thru: GET URL: ${this._cliente.url.hostname}${path}`);

    this._cliente.get(path, callback);
}

ConexaoService.prototype.aceitarConexao = function(conexaoID, postData, callback) {

    let path = '/api/Conexao/AceitarConexao?conexaoID=' + conexaoID;

    console.log(`Chamando api Driver-Thru: POST URL: ${this._cliente.url.hostname}${path} com o seguinte body: ${JSON.stringify(postData)}`);

    this._cliente.post(path, postData, callback);
}

ConexaoService.prototype.encerrarConexao = function(conexaoID, postData, callback) {

    let path = '/api/Conexao/EncerrarConexao?conexaoID=' + conexaoID;

    console.log(`Chamando api Driver-Thru: POST URL: ${this._cliente.url.hostname}${path} com o seguinte body: ${JSON.stringify(postData)}`);

    this._cliente.post(path, postData, callback);
}

module.exports = function() {
    return ConexaoService;
}