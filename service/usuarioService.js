var restify = require('restify-clients');

function UsuarioService() {
    this._cliente = restify.createJsonClient({
        url: `http://webapidtv.azurewebsites.net`
    });
    this._cliente.basicAuth('AppV3rdtUs3r', 'Vj-Nsd@f6gh#541c%jMx*MlY');
}

UsuarioService.prototype.usuarioEstabelecimento = function(userID, callback) {

    let path = '/api/Usuario/UsuarioEstabelecimento?usuarioID=' + userID;

    console.log(`Chamando api Driver-Thru: GET URL: ${this._cliente.url.hostname}${path}`);

    this._cliente.get(path, callback);
}

UsuarioService.prototype.usuario = function(userID, callback) {

    let path = '/api/Usuario/' + userID;

    console.log(`Chamando api Driver-Thru: GET URL: ${this._cliente.url.hostname}${path}`);

    this._cliente.get(path, callback);
}

module.exports = function() {
    return UsuarioService;
}