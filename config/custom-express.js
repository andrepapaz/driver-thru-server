var express = require('express');
var consign = require('consign');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
var cors = require('cors');

module.exports = () => {
    var app = express();

    app.use(bodyParser.json());
    app.use(expressValidator());
    app.use(cors({ credentials: true, origin: true }));

    consign()
        .include('controllers')
        .include('service')
        .into(app);

    return app;
}