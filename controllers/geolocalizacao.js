const config = require('../config/env/config');

module.exports = (app) => {
    app.post('/api/GeoLocalizacao/InserirGeolocalizacao', (req, res) => {

        let postdata = req.body;

        if (!config.mockado) {
            var geolocalizacaoService = new app.service.geolocalizacaoService();

            geolocalizacaoService.inserirGeolocalizacao(postdata, function(err, reqApi, resApi, data) {

                if (err) {
                    res.status(500);
                    return;
                }

                res.json(data);
            });
        } else {
            res.json({ "QtdeConexoes": 1 });
        }
    });

}