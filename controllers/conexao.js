const config = require('../config/env/config');

module.exports = (app) => {

    app.get('/api/Conexao/ListarConexoesAtivas', (req, res) => {

        let usuarioID = req.query.usuarioID;

        if (!config.mockado) {
            var conexaoService = new app.service.conexaoService();

            conexaoService.listarConexoesAtivas(usuarioID, function(err, reqApi, resApi, data) {

                if (err) {
                    res.status(500);
                    return;
                }

                res.json(data);
            });
        } else {
            res.json({
                "Conexao": [{
                        "ConexaoID": 532,
                        "OrigemUsuarioID": 153,
                        "DestinoUsuarioID": 143,
                        "DataHoraCriacao": "2018-06-16T09:30:21.557",
                        "DataHoraInicio": null,
                        "DataHoraAceite": null,
                        "DataHoraFim": null,
                        "StatusConexaoID": 1,
                        "NumeroPin": 4,
                        "CampoLivre": "MOCK Outro Bolo Ainda Maior",
                        "QtdRequestsGoogleDS": null,
                        "NomeDestino": "Margaret Store",
                        "Latitude": "-23.628415",
                        "Longitude": "-46.7072298"
                    },
                    {
                        "ConexaoID": 522,
                        "OrigemUsuarioID": 153,
                        "DestinoUsuarioID": 143,
                        "DataHoraCriacao": "2018-06-16T08:59:57.917",
                        "DataHoraInicio": null,
                        "DataHoraAceite": null,
                        "DataHoraFim": null,
                        "StatusConexaoID": 1,
                        "NumeroPin": 3,
                        "CampoLivre": "MOCK Outro Bolo Maior",
                        "QtdRequestsGoogleDS": null,
                        "NomeDestino": "Margaret Store",
                        "Latitude": "-23.628415",
                        "Longitude": "-46.7072298"
                    },
                    {
                        "ConexaoID": 521,
                        "OrigemUsuarioID": 153,
                        "DestinoUsuarioID": 143,
                        "DataHoraCriacao": "2018-06-16T08:59:37.36",
                        "DataHoraInicio": "2018-06-16T09:00:04.003",
                        "DataHoraAceite": null,
                        "DataHoraFim": null,
                        "StatusConexaoID": 2,
                        "NumeroPin": 2,
                        "CampoLivre": "MOCK Outro Bolo",
                        "QtdRequestsGoogleDS": null,
                        "NomeDestino": "Margaret Store",
                        "Latitude": "-23.628415",
                        "Longitude": "-46.7072298"
                    },
                    {
                        "ConexaoID": 504,
                        "OrigemUsuarioID": 153,
                        "DestinoUsuarioID": 143,
                        "DataHoraCriacao": "2018-06-15T22:38:04.757",
                        "DataHoraInicio": "2018-06-15T22:38:17.82",
                        "DataHoraAceite": "2018-06-15T21:38:32.927",
                        "DataHoraFim": null,
                        "StatusConexaoID": 3,
                        "NumeroPin": 1,
                        "CampoLivre": "MOCK Retirar bolo",
                        "QtdRequestsGoogleDS": null,
                        "NomeDestino": "Margaret Store",
                        "Latitude": "-23.628415",
                        "Longitude": "-46.7072298"
                    }
                ]
            });
        }

    });

    app.post('/api/Conexao/AceitarConexao', (req, res) => {

        let conexaoID = req.query.conexaoID;

        let postData = req.body;

        if (!config.mockado) {
            var conexaoService = new app.service.conexaoService();

            conexaoService.aceitarConexao(conexaoID, postData, function(err, reqApi, resApi, data) {

                if (err) {
                    res.status(500).send(err.message);
                    return;
                }

                res.json(data);
            });
        } else {
            res.json({
                "ConexoesAceitas": 3
            });
        }

    });

    app.post('/api/Conexao/EncerrarConexao', (req, res) => {

        let conexaoID = req.query.conexaoID;

        let postData = req.body;

        if (!config.mockado) {
            var conexaoService = new app.service.conexaoService();

            conexaoService.encerrarConexao(conexaoID, postData, function(err, reqApi, resApi, data) {

                if (err) {
                    res.status(500).send(err.message);
                    return;
                }

                res.json(data);
            });
        } else {
            res.json({});
        }
    });

}